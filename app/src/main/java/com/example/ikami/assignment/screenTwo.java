package com.example.ikami.assignment;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class screenTwo extends AppCompatActivity {
    Button backButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_screen_two);
        onClickButtonListener();
    }
    public void onClickButtonListener(){
        backButton=(Button) findViewById(R.id.backbutton2);
        backButton.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent returnButton= new Intent(getApplicationContext(), MainActivity.class);
                        startActivity(returnButton);
                    }

                }
        );
    }
}
