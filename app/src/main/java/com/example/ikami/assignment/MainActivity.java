package com.example.ikami.assignment;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {
 Button firstButton;
 Button secondButton;
 Button thirdButton;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        onClickButtonListener();
        addButtonListener();
        addOnClickButtonListener();
    }
    public void onClickButtonListener() {
       firstButton = (Button) findViewById(R.id.firstbutton);
        firstButton.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent("com.example.ikami.comp496assignment.screen2");
                        startActivity(intent);
                    }

                }
        );
    }

    public void addButtonListener() {
        secondButton = (Button) findViewById(R.id.secondbutton);
        secondButton.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent("com.example.ikami.comp496assignment.screen3");
                        startActivity(intent);
                    }

                }
        );
    }
    public void addOnClickButtonListener(){
        thirdButton=(Button)findViewById(R.id.thirdbutton);
        thirdButton.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent= new Intent("com.example.ikami.comp496assignment.screen4");
                        startActivity(intent);
                    }

                }
        );
    }
}
