package com.example.ikami.assignment;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class screenFour extends AppCompatActivity {
    Button backButton3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_screen_four);
        onClickButtonListener();
    }
    public void onClickButtonListener(){
        backButton3=(Button) findViewById(R.id.backbutton4);
        backButton3.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent returnButton= new Intent(getApplicationContext(), MainActivity.class);
                        startActivity(returnButton);
                    }

                }
        );
    }
}
