package com.example.ikami.assignment;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class screenThree extends AppCompatActivity {
Button backButton2;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_screen_three);
        onClickButtonListener();
    }
    public void onClickButtonListener(){
        backButton2=(Button) findViewById(R.id.backbutton3);
        backButton2.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent returnButton= new Intent(getApplicationContext(), MainActivity.class);
                        startActivity(returnButton);
                    }

                }
        );
    }
}
